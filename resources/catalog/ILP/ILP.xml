<?xml version="1.0" encoding="UTF-8"?>
<Pipeline xmlns="http://nrg.wustl.edu/pipeline" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://nrg.wustl.edu/pipeline ..\..\schema\pipeline.xsd" xmlns:ext="org.nrg.validate.utils.ProvenanceUtils" xmlns:fileUtils="http://www.xnat.org/java/org.nrg.imagingtools.utils.FileUtils">
    <name>ILP</name>
    <location>ILP</location>
    <description>Individual Longitudinal Participant - Compares ICV-normalized hippocampal and lateral ventricle volumes.</description>
    <resourceRequirements>
        <property name="DRMAA_JobTemplate_JobCategory">ilp_q</property>
    </resourceRequirements>
    <documentation>
        <authors>
            <author>
                <lastname>Flavin</lastname>
                <firstname>John</firstname>
                <contact>
                    <email>flavinj@mir.wustl.edu</email>
                </contact>
            </author>
        </authors>
        <version>1</version>
        <input-parameters>
            <parameter>
                <name>age</name>
                <description>Patient age at time of study</description>
            </parameter>
        </input-parameters>
    </documentation>
    <xnatInfo appliesTo="fs:fsData" />
    <outputFileNamePrefix>^concat(/Pipeline/parameters/parameter[name='logdir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='label']/values/unique/text())^</outputFileNamePrefix>
    <parameters>
        <parameter>
            <name>workdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='builddir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='label']/values/unique/text())^</unique>
            </values>
        </parameter>
        <parameter>
            <name>logdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/LOGS')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>mrId_query</name>
            <values>
                <unique>^fileUtils:GetColumn(/Pipeline/parameters/parameter[name='host']/values/unique/text(), /Pipeline/parameters/parameter[name='user']/values/unique/text(), /Pipeline/parameters/parameter[name='pwd']/values/unique/text(), concat('/data/experiments/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'?handler=values&amp;format=json&amp;columns=imagesession_id'), 'imagesession_id')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>mrId</name>
            <values>
                <unique>^if( if(boolean(/Pipeline/parameters/parameter[name='mrId_query'])) then /Pipeline/parameters/parameter[name='mrId_query']/values/unique/text()!='' else false() ) then /Pipeline/parameters/parameter[name='mrId_query']/values/unique/text() else substring-before(/Pipeline/parameters/parameter[name='id']/values/unique/text(),'_freesurfer_')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>mrLabel</name>
            <values>
                <unique>^fileUtils:GetColumn(/Pipeline/parameters/parameter[name='host']/values/unique/text(), /Pipeline/parameters/parameter[name='user']/values/unique/text(), /Pipeline/parameters/parameter[name='pwd']/values/unique/text(), concat('/data/experiments/',/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'?handler=values&amp;format=json&amp;columns=label'), 'label')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>mrScan_query</name>
            <values>
                <unique>^fileUtils:GetColumn(/Pipeline/parameters/parameter[name='host']/values/unique/text(), /Pipeline/parameters/parameter[name='user']/values/unique/text(), /Pipeline/parameters/parameter[name='pwd']/values/unique/text(), concat('/data/experiments/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'?handler=values&amp;format=json&amp;columns=parameters/addParam%5Bname=INCLUDED_T1%5D/addField'), 'parameters/addParam[name=INCLUDED_T1]/addField')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>mrScan</name>
            <values>
                <unique>^if( if(boolean(/Pipeline/parameters/parameter[name='mrScan_query'])) then /Pipeline/parameters/parameter[name='mrScan_query']/values/unique/text()!='' else false() ) then if(contains(/Pipeline/parameters/parameter[name='mrScan_query']/values/unique/text(),' ')) then substring-before(/Pipeline/parameters/parameter[name='mrScan_query']/values/unique/text(),' ') else /Pipeline/parameters/parameter[name='mrScan_query']/values/unique/text() else ''^</unique>
            </values>
        </parameter>
        <parameter>
            <name>mrScan_hasDicom</name>
            <values>
                <unique>^if(/Pipeline/parameters/parameter[name='mrScan']/values/unique/text()='') then false() else fileUtils:GetColumn(/Pipeline/parameters/parameter[name='host']/values/unique/text(), /Pipeline/parameters/parameter[name='user']/values/unique/text(), /Pipeline/parameters/parameter[name='pwd']/values/unique/text(), concat('/data/experiments/',/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'/scans/',/Pipeline/parameters/parameter[name='mrScan']/values/unique/text(),'?handler=values&amp;format=json&amp;columns=file%5Blabel=DICOM%5D/label'), 'file[label=DICOM]/label')='DICOM'^</unique>
            </values>
        </parameter>
        <parameter>
            <name>fsVersionString</name>
            <values>
                <unique>^fileUtils:GetColumn(/Pipeline/parameters/parameter[name='host']/values/unique/text(), /Pipeline/parameters/parameter[name='user']/values/unique/text(), /Pipeline/parameters/parameter[name='pwd']/values/unique/text(), concat('/data/experiments/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'?handler=values&amp;format=json&amp;columns=fs_version'), 'fs_version')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>fsVersion</name>
            <values>
                <unique>^if (ends-with(/Pipeline/parameters/parameter[name='fsVersionString']/values/unique/text(),'v5.1.0')) then '5.1' else if (ends-with(/Pipeline/parameters/parameter[name='fsVersionString']/values/unique/text(),'v5.3.0-HCP-patch')) then '5.3-HCP-patch' else if (ends-with(/Pipeline/parameters/parameter[name='fsVersionString']/values/unique/text(),'v5.3.0-HCP')) then '5.3-HCP' else 'XX'^</unique>
            </values>
        </parameter>
        <parameter>
            <name>rawdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/RAW')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>freesurfer_subjectdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='rawdir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='mrLabel']/values/unique/text())^</unique>
            </values>
        </parameter>
        <parameter>
            <name>freesurfer_statsdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='freesurfer_subjectdir']/values/unique/text(),'/stats')^</unique>
            </values>
            <description>Freesurfer stats dir</description>
        </parameter>
        <parameter>
            <name>processeddir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/PROCESSED')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>dicomindir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='rawdir']/values/unique/text(),'/DICOM')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>dicomoutdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='processeddir']/values/unique/text(),'/DICOM')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>snapshotdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='processeddir']/values/unique/text(),'/png')^</unique>
            </values>
        </parameter>
    </parameters>
    <steps>
        <step id="MKDIR" description="Prepare for Freesurfer">
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='freesurfer_statsdir']/values/unique/text()^</value>
                </argument>
            </resource>
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='processeddir']/values/unique/text()^</value>
                </argument>
            </resource>
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='snapshotdir']/values/unique/text()^</value>
                </argument>
            </resource>
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='dicomindir']/values/unique/text()^</value>
                </argument>
            </resource>
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='dicomoutdir']/values/unique/text()^</value>
                </argument>
            </resource>
        </step>
        <step id="GET_ASSESSOR" description="Pull down the existing Freesurfer assessor" workdirectory="^/Pipeline/parameters/parameter[name='rawdir']/values/unique/text()^">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="method">
                    <value>GET</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'/assessors/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'?format=xml"')^</value>
                </argument>
                <argument id="outfile">
                    <value>FS.xml</value>
                </argument>
            </resource>
        </step>
        <step id="GET_APARC_ASEG_STATS" description="Get Freesurfer stats files" workdirectory="^/Pipeline/parameters/parameter[name='freesurfer_statsdir']/values/unique/text()^">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="method">
                    <value>GET</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'/assessors/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'/resources/DATA/files/',/Pipeline/parameters/parameter[name='mrLabel']/values/unique/text(),'/stats/aseg.stats"')^</value>
                </argument>
                <argument id="outfile">
                    <value>aseg.stats</value>
                </argument>
            </resource>
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="method">
                    <value>GET</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'/assessors/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'/resources/DATA/files/',/Pipeline/parameters/parameter[name='mrLabel']/values/unique/text(),'/stats/lh.aparc.stats"')^</value>
                </argument>
                <argument id="outfile">
                    <value>lh.aparc.stats</value>
                </argument>
            </resource>
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="method">
                    <value>GET</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'/assessors/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'/resources/DATA/files/',/Pipeline/parameters/parameter[name='mrLabel']/values/unique/text(),'/stats/rh.aparc.stats"')^</value>
                </argument>
                <argument id="outfile">
                    <value>rh.aparc.stats</value>
                </argument>
            </resource>
        </step>
        <step id="GET_SCAN" description="Download the DICOMs on which the FS was originally run" workdirectory="^/Pipeline/parameters/parameter[name='dicomindir']/values/unique/text()^" precondition="^/Pipeline/parameters/parameter[name='mrScan_hasDicom']/values/unique/text()='true'^">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="absolutePath"/>
                <argument id="batch" />
                <argument id="method">
                    <value>GET</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'/scans/',/Pipeline/parameters/parameter[name='mrScan']/values/unique/text(),'/resources/DICOM/files"')^</value>
                </argument>
            </resource>
        </step>
        <step id="MAKE_SESSIONS_CSV" description="Pull all session info for a patient from XML, based on a session number" workdirectory="^/Pipeline/parameters/parameter[name='rawdir']/values/unique/text()^">
            <resource name="Check_prior_sessions" location="ILP/resources">
                <argument id="mrId">
                    <value>^/Pipeline/parameters/parameter[name='mrId']/values/unique/text()^</value>
                </argument>
                <argument id="host">
                    <value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
                </argument>
                <argument id="user">
                    <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                </argument>
                <argument id="pwd">
                    <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                </argument>
            </resource>
        </step>        
        <step id="MAKE_STATS_CSV" description="Turn the aseg stats into a CSV" workdirectory="^/Pipeline/parameters/parameter[name='rawdir']/values/unique/text()^">
            <resource name="FS_stats_maker" location="ILP/resources">
                <argument id="agelist">
                    <value>^concat(/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'_age_list.csv')^</value>
                </argument>
                <argument id="sesslist">
                    <value>^concat(/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'_session_list.csv')^</value>
                </argument>
                <argument id="outfile">
                    <value>stats.csv</value>
                </argument>
                <argument id="fsVersion">
                    <value>^/Pipeline/parameters/parameter[name='fsVersion']/values/unique/text()^</value>
                </argument>
            </resource>
        </step>
        <step id="ILP" description="Run ILP script" workdirectory="^/Pipeline/parameters/parameter[name='processeddir']/values/unique/text()^">
            <resource name="standard_ILP_regions" location="ILP/resources">
                <argument id="volumeCSV">
                    <value>^concat(/Pipeline/parameters/parameter[name='rawdir']/values/unique/text(),'/stats.csv')^</value>
                </argument>
                <argument id="sessionLabel">
                    <value>^/Pipeline/parameters/parameter[name='mrLabel']/values/unique/text()^</value>
                </argument>
                <argument id="supernorm">
                    <value>@PIPELINE_DIR_PATH@/catalog/ILP/resources/oasis_and_dian_normals.csv</value>
                </argument>
            </resource>
        </step>
        <step id="MV_SNAP" description="Move the snapshots to their directory">
            <resource name="mv" location="commandlineTools">
                <argument id="source">
                    <value>^concat(/Pipeline/parameters/parameter[name='processeddir']/values/unique/text(),'/*.png')^</value>
                </argument>
                <argument id="destination">
                    <value>^/Pipeline/parameters/parameter[name='snapshotdir']/values/unique/text()^</value>
                </argument>
            </resource>
        </step>
        <step id="MODIFY_ASSESSOR" description="Add ILP stats to FS assessor XML" workdirectory="^/Pipeline/parameters/parameter[name='processeddir']/values/unique/text()^">
            <resource name="ilp_stats_to_assessor" location="ILP/resources">
                <argument id="age">
                    <value>^/Pipeline/parameters/parameter[name='age']/values/unique/text()^</value>
                </argument>
                <argument id="FSXML">
                    <value>^concat(/Pipeline/parameters/parameter[name='rawdir']/values/unique/text(),'/FS.xml')^</value>
                </argument>
                <argument id="ilpStats">
                    <value>^concat(/Pipeline/parameters/parameter[name='mrLabel']/values/unique/text(),'_normalized_stats_and_z.csv')^</value>
                </argument>
                <argument id="FSROICSV">
                    <value>@PIPELINE_DIR_PATH@/catalog/ILP/resources/ROI_list.csv</value>
                </argument>
                <argument id="assessorOut">
                    <value>ILP.xml</value>
                </argument>
            </resource>
        </step>
        <step id="PUT_ASSESSOR" description="Overwrite FS assessor with ILP assessor" workdirectory="^/Pipeline/parameters/parameter[name='processeddir']/values/unique/text()^">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="method">
                    <value>PUT</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'/data/experiments/',/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'/assessors/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'?overwrite=true&amp;inbody=true&amp;event_id=',/Pipeline/parameters/parameter[name='workflowid']/values/unique/text(),'"')^</value>
                </argument>
                <argument id="infile">
                    <value>ILP.xml</value>
                </argument>
            </resource>
        </step>
        <step id="PUT_ILP_SNAPSHOTS" description="PUT the ILP images to a FS resource">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="method">
                    <value>PUT</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'/data/experiments/',/Pipeline/parameters/parameter[name='mrId']/values/unique/text(),'/assessors/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'/resources/ILP_IMAGES/files?format=PNG&amp;overwrite=true&amp;reference=',/Pipeline/parameters/parameter[name='snapshotdir']/values/unique/text(),'&amp;event_id=',/Pipeline/parameters/parameter[name='workflowid']/values/unique/text(),'"')^</value>
                </argument>
            </resource>
        </step>
        <step id="MAKE_DICOM" description="Convert ILP output images to DICOM" workdirectory="^/Pipeline/parameters/parameter[name='snapshotdir']/values/unique/text()^" precondition="^/Pipeline/parameters/parameter[name='mrScan_hasDicom']/values/unique/text()='true'^">
            <resource name="ilpqc2dicom" location="ILP/resources">
                <argument id="seriesBase">
                    <value>^/Pipeline/parameters/parameter[name='mrScan']/values/unique/text()^</value>
                </argument>
                <argument id="dicomInDir">
                    <value>^/Pipeline/parameters/parameter[name='dicomindir']/values/unique/text()^</value>
                </argument>
                <argument id="dicomOutDirBase">
                    <value>^/Pipeline/parameters/parameter[name='dicomoutdir']/values/unique/text()^</value>
                </argument>
            </resource>
        </step>
        <step id="PUT_SYNTHETIC_DICOM" description="Upload synthetic DICOM images as scans to the XNAT experiment" workdirectory="^/Pipeline/parameters/parameter[name='dicomoutdir']/values/unique/text()^" precondition="^/Pipeline/parameters/parameter[name='mrScan_hasDicom']/values/unique/text()='true'^">
            <resource name="upload-scans" location="pipeline-tools">
                <argument id="sessionId">
                    <value>^/Pipeline/parameters/parameter[name='mrId']/values/unique/text()^</value>
                </argument>
                <argument id="host">
                    <value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
                </argument>
                <argument id="username">
                    <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                </argument>
                <argument id="password">
                    <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                </argument>
                <argument id="workflowId">
                    <value>^/Pipeline/parameters/parameter[name='workflowid']/values/unique/text()^</value>
                </argument>
                <argument id="delete">
                    <value>True</value>
                </argument>
                <argument id="regenerateMetadata">
                    <value>True</value>
                </argument>
            </resource>
        </step>
    </steps>
</Pipeline>
