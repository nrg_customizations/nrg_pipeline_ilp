#!/bin/bash -xe


AGELIST=$1
SESSLIST=$2
OUTFILE=$3
FS_VERSION=$4
# AGELIST should be a csv that contains ages
# SESSLIST should be a csv that contains sessions
# both should be ordered the same way - i.e. largest to smallest age, largest to smallest session number


case $FS_VERSION in
    5.1)
    SETUP=freesurfer5_setup.sh
    ;;
    5.3-HCP)
    SETUP=freesurfer53-HCP_setup.sh
    ;;
    5.3-HCP-patch)
    SETUP=freesurfer53-patch_setup.sh
    ;;
    *)
    echo "I don't recognize FreeSurfer version $FS_VERSION"
    exit 1
    ;;
esac

export SUBJECTS_DIR=`pwd`

#check that input exists
# commented out for now. need to do this check for each subject in $SESSLIST
#if [ ! -d "${SUBJECTS_DIR}/${SUBJECT}" ]; then
#    echo "Directory $SUBJECT must exist within SUBJECTS_DIR $SUBJECTS_DIR"
#    exit 1
#fi

# Set up Freesurfer
source @PIPELINE_DIR_PATH@/scripts/$SETUP
#source /data/tip/pipeline/scripts/$SETUP


###### CREATE SESSION LIST THAT FREESURFER CAN USE ######

# Get the first number in $SUBLIST to use as the session name (for naming temporary csv files)

SESSIONNAME=$(head -q -n 1 $SESSLIST)
SESSIONNAME=$(echo $SESSIONNAME|tr -d '\r ')

# Convert SESSLIST to something that FreeSurfer can use

subjects=$(tr '\n\r' ' ' <${SESSLIST})

# Get each line in the csv, put it into a txt file

# first clear ${SESSIONNAME}_list.txt
#truncate -s 0 ${SESSIONNAME}_list.txt

#while read f1
#do
#    echo $f1 | cut -d ' ' >> ${SESSIONNAME}_list.txt
#done < $SESSLIST

###### OUTPUT ALL STATS TRANSPOSED #######

#####ASEG######
asegstats2table --subjects $subjects --delimiter comma --all-segs --skip -t ${SESSIONNAME}_aseg.csv

#####APARC######
#volumes LH
aparcstats2table --hemi lh --subjects $subjects --delimiter comma --skip --measure volume -t ${SESSIONNAME}_vol_lh.csv

#volumes RH
aparcstats2table --hemi rh --subjects $subjects --delimiter comma --skip --measure volume -t ${SESSIONNAME}_vol_rh.csv


#thicknesses LH
aparcstats2table --hemi lh --subjects $subjects --delimiter comma --skip --measure thickness -t ${SESSIONNAME}_thick_lh.csv

#thicknesses RH
aparcstats2table --hemi rh --subjects $subjects --delimiter comma --skip --measure thickness -t ${SESSIONNAME}_thick_rh.csv

######PUT EVERYTHING TOGETHER#######
#cut off the first columns of all the files except the first one
cut -d ',' -f 2- ${SESSIONNAME}_vol_lh.csv > temp1.csv
cut -d ',' -f 2- ${SESSIONNAME}_vol_rh.csv > temp2.csv
cut -d ',' -f 2- ${SESSIONNAME}_thick_lh.csv > temp3.csv
cut -d ',' -f 2- ${SESSIONNAME}_thick_rh.csv > temp4.csv
cut -d ',' -f 2- ${AGELIST} > temp5.csv

#paste everything, including ages csv
#paste -d, ${SESSIONNAME}_aseg.csv temp1.csv temp2.csv temp3.csv temp4.csv $AGELIST > ${SESSIONNAME}_FS_stats.csv
paste -d, ${SESSIONNAME}_aseg.csv temp1.csv temp2.csv temp3.csv temp4.csv temp5.csv > ${OUTFILE}

###### HEADER TRANSFORMATIONS #######
#sed -i -e '1 s/Measure:volume/Session/' -e '1 s/-/_/g' -e '1 s/3rd/third/' -e '1 s/4th/fourth/' -e '1 s/5th/fifth/' -e '1 s/EstimatedTotal//' ${SESSIONNAME}_FS_stats.csv

sed -i -e '1 s/Measure:volume/Session/' -e '1 s/-/_/g' -e '1 s/3rd/third/' -e '1 s/4th/fourth/' -e '1 s/5th/fifth/' -e '1 s/EstimatedTotal//' ${OUTFILE}

#change the first heading to 'session'
#sed -i 's/Measure:volume/Session/g' ${SESSIONNAME}_FS_stats.csv

sed -i 's/Measure:volume/Session/g' ${OUTFILE}

#get rid of temp files
rm temp1.csv temp2.csv temp3.csv temp4.csv temp5.csv ${SESSIONNAME}_vol_lh.csv ${SESSIONNAME}_vol_rh.csv ${SESSIONNAME}_thick_lh.csv ${SESSIONNAME}_thick_rh.csv
